<?php

require __DIR__ . '/../src/Lib/psr4AutoloaderClass.php';
use App\Covoiturage\Controleur\ControleurUtilisateur;

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

// Définir le contrôleur et l'action par défaut
$action = 'AfficherListe';
$nomDeClasseControleur = '';

// Vérifier si 'controleur' est défini et construire le nom de la classe du contrôleur
if (isset($_REQUEST['controleur'])) {
    $controleur = ucfirst($_REQUEST['controleur']);
    $nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur" . $controleur;
} else if (\App\Covoiturage\Lib\PreferenceControleur::existe()){
    $nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur" . ucfirst(\App\Covoiturage\Lib\PreferenceControleur::lire());
} else {
    $nomDeClasseControleur = "App\Covoiturage\Controleur\ControleurUtilisateur";
}

// Vérifier si la classe du contrôleur existe, sinon afficher une erreur
if (!class_exists($nomDeClasseControleur)) {
    $nomDeClasseControleur = 'App\Covoiturage\Controleur\ControleurUtilisateur';
    echo 'ratio1';
    $action = 'AfficherErreur';
} else {
    // Si l'action est définie, la vérifier et l'utiliser
    if (isset($_REQUEST['action'])) {
        $action = $_REQUEST['action'];
        // Si l'action n'est pas une méthode de la classe, afficher une erreur
        if (!method_exists($nomDeClasseControleur, $action)) {
            echo 'ratio2';
            $action = 'AfficherErreur';
        }

    }
}

// Appeler l'action sur le contrôleur déterminé
$nomDeClasseControleur::$action();



?>