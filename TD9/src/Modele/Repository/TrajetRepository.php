<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use DateTime;

class TrajetRepository extends AbstractRepository
{
    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        /** @var Trajet $objet */
        return array(
            "idTag" => $objet->getId(),
            "departTag" => $objet->getDepart(),
            "arriveeTag" => $objet->getArrivee(),
            "dateTag" => $objet->getDate()->format("Y-m-d"),
            "prixTag" => $objet->getPrix(),
            "conducteurTag" => $objet->getConducteur()->getLogin(),
            "nonFumeurTag" => $objet->isNonFumeur() ? 1 : 0
        );
    }

    protected function getNomColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteur", "nonFumeur"];
    }

    protected function getNomClePrimaire(): string {
        return 'id';
    }

    protected function getNomTable() : string
    {
        return 'trajet';
    }

    protected function construireDepuisTableauSQL(array $objetFormatTableau): Trajet
    {
        return new Trajet(
            $objetFormatTableau["id"],
            $objetFormatTableau["depart"],
            $objetFormatTableau["arrivee"],
            new DateTime($objetFormatTableau["date"]), // À changer
            $objetFormatTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($objetFormatTableau["conducteur"]), // À changer
            $objetFormatTableau["nonFumeur"], // À changer ?
        );
    }

    /*
    public static function recupererTrajets(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach ($pdoStatement as $trajetFormatTableau) {
            $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }
    */

    private static function recupererPassagers(Trajet $trajet): array
    {
        $sql = "SELECT * FROM utilisateur WHERE login IN (SELECT passagerLogin FROM passager WHERE trajetId = :trajetTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);

        $values = array(
            "trajetTag" => $trajet->getId()
        );
        $pdoStatement->execute($values);

        foreach ($pdoStatement as $passagerFormatTableau) {
            $trajet->setPassagers((array)(new UtilisateurRepository())->construireDepuisTableauSQL($passagerFormatTableau));
        }

        return $trajet->getPassagers();
    }


}