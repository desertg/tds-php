<?php

namespace App\Covoiturage\Controleur;


use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet extends ControleurGenerique
{
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['trajets' => $trajets, 'titre' => 'Liste des trajets', 'cheminCorpsVue' => 'trajet/liste.php']); //appel à la vue
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue('vueGenerale.php', ['messageErreur' => $messageErreur, 'titre' => 'Problème de trajet', 'cheminCorpsVue' => 'trajet/erreur.php']);
    }

    public static function afficherDetail(): void
    {
        $id = $_REQUEST['trajetId'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if ($trajet == null) {
            self::afficherErreur("Trajet inconnu");
        } else {
            self::afficherVue('vueGenerale.php', ['trajet' => $trajet, 'titre' => 'Détails trajet', 'cheminCorpsVue' => 'trajet/detail.php']);
        }
    }

    public static function supprimer(): void
    {
        $id = $_REQUEST['trajetId'];
        (new TrajetRepository())->supprimerParClePrimaire($id);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ['titre' => 'Trajet supprimé', 'cheminCorpsVue' => 'trajet/trajetSupprime.php', 'id' => $id, "trajets" => $trajets]);
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ['titre' => 'Créer trajet', 'cheminCorpsVue' => 'trajet/formulaireCreation.php']);
    }

    public static function creerDepuisFormulaire(): void
    {
        $trajet = self::construireDepuisFormulaire($_REQUEST);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ['titre' => 'Trajet créé', 'cheminCorpsVue' => 'trajet/trajetCree.php', 'trajets' => $trajets]);
    }

    public static function afficherFormulaireMiseAJour(): void {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_REQUEST['trajetId']);
        self::afficherVue("vueGenerale.php", ['titre' => 'Modifier trajet', 'cheminCorpsVue' => 'trajet/formulaireMiseAJour.php', 'trajet' => $trajet]);
    }

    public static function mettreAJour(): void
    {
        $trajet = self::construireDepuisFormulaire($_REQUEST);
        (new TrajetRepository())->mettreAJour($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ['titre' => 'Trajet mis à jour', 'cheminCorpsVue' => 'trajet/trajetMisAJour.php', 'trajets' => $trajets]);
    }

    /**
     * @return Trajet
     * @throws \DateMalformedStringException
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire['trajetId'] ?? null;
        $depart = $tableauDonneesFormulaire["depart"];
        $arrivee = $tableauDonneesFormulaire["arrivee"];
        $date = $tableauDonneesFormulaire["date"];
        $prix = $tableauDonneesFormulaire["prix"];
        $conducteur = $tableauDonneesFormulaire["conducteurLogin"];
        $nonFumeur = isset($tableauDonneesFormulaire["nonFumeur"]);
        $trajet = new Trajet($id, $depart, $arrivee, new DateTime($date), $prix, (new UtilisateurRepository())->recupererParClePrimaire($conducteur), $nonFumeur);
        return $trajet;
    }


}