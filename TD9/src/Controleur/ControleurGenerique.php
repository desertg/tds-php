<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{

    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        $messagesFlash = MessageFlash::lireTousMessages();
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulairePreference(): void
    {
        self::afficherVue('vueGenerale.php', ['titre' => 'Préférences', 'cheminCorpsVue' => 'formulairePreference.php']);
    }


    public static function enregistrerPreference(): void
    {
        $controleur = $_REQUEST['controleur_defaut'];
        PreferenceControleur::enregistrer($controleur);
        MessageFlash::ajouter("success", "Préférence enregistrée !");
        self::redirectionVersUrl("controleurFrontal.php");
    }

    public static function redirectionVersUrl($url): void {
        header("Location: $url");
        exit();
    }
}