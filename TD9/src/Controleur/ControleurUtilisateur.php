<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, 'titre' => 'Liste des utilisateurs', 'cheminCorpsVue' => 'utilisateur/liste.php']); //appel à la vue
    }

    public static function afficherDetail(): void
    {
        $login = $_REQUEST['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur == null) {
            MessageFlash::ajouter("warning", "Login incorrect");
            self::redirectionVersUrl("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
        } else {
            self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, 'titre' => 'Détails utilisateur', 'cheminCorpsVue' => 'utilisateur/detail.php']);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ['titre' => 'Créer utilisateur', 'cheminCorpsVue' => 'utilisateur/formulaireCreation.php']);
    }

    public static function creerDepuisFormulaire(): void
    {
        if ($_REQUEST['mdp2'] !== $_REQUEST['mdp']) {
            MessageFlash::ajouter("warning", "Les mots de passes sont distincts");
            self::redirectionVersUrl("controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur");
            return;
        }

        if (isset($_REQUEST["nom"], $_REQUEST["prenom"], $_REQUEST["mdp"], $_REQUEST["mdp2"], $_REQUEST["email"]) && filter_var($_REQUEST["email"], FILTER_VALIDATE_EMAIL)) {

            $utilisateur = self::construireDepuisFormulaire($_REQUEST);
            (new UtilisateurRepository())->ajouter($utilisateur);
            VerificationEmail::envoiEmailValidation($utilisateur);
            MessageFlash::ajouter("success", "Utilisateur créé !");
            self::redirectionVersUrl("controleurFrontal.php?action=afficherDetail&login=" . $utilisateur->getLogin() ."&controleur=utilisateur");
            return;

        }
        self::afficherErreur("Champs manquant");
    }


    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue('vueGenerale.php', ['messageErreur' => $messageErreur, 'titre' => 'Problème utilisateur', 'cheminCorpsVue' => 'utilisateur/erreur.php']);
    }

    public static function supprimer(): void
    {
        $login = $_REQUEST['login'];

        if (!ConnexionUtilisateur::estUtilisateur($login)) {
            self::afficherErreur("La suppression n'est possible que pour l'utilisateur connecté");
            return;
        }
        ConnexionUtilisateur::deconnecter();
        (new UtilisateurRepository())->supprimerParClePrimaire($login);
        MessageFlash::ajouter("success", "Utilisateur supprimé");
        self::redirectionVersUrl("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
        if ($utilisateur === null && ConnexionUtilisateur::estAdministrateur()) {
            MessageFlash::ajouter("warning", "Login inconnu");
            self::redirectionVersUrl("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
            return;
        }
        if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin()) || ConnexionUtilisateur::estAdministrateur()) {
            self::afficherVue("vueGenerale.php", ['titre' => 'Modifier utilisateur', 'cheminCorpsVue' => 'utilisateur/formulaireMiseAJour.php', 'utilisateur' => $utilisateur]);
            return;
        }
        MessageFlash::ajouter("danger", "La mise à jour n'est possible que pour l'utilisateur connecté");
        self::redirectionVersUrl("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
    }

    public static function mettreAJour(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);

        if (!ConnexionUtilisateur::estConnecte() ||
            (!ConnexionUtilisateur::estAdministrateur() && !ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin()))) {
            MessageFlash::ajouter("warning", "Modification autorisée uniquement pour soi-même ou un admin");
            self::redirectionVersUrl("controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur");
            return;
        }


        if ($_REQUEST['mdp'] !== $_REQUEST['mdp2']) {
            MessageFlash::ajouter("warning", "Les mots de passes sont distincts");
            self::redirectionVersUrl("controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur");
            return;
        }

        if (ConnexionUtilisateur::estConnecte() && !ConnexionUtilisateur::estAdministrateur()) {
            if (!MotDePasse::verifier($_REQUEST['oldMdp'], $utilisateur->getMdpHache())) {
                MessageFlash::ajouter("warning", "Ancien mot de passe incorrect");
                self::redirectionVersUrl("controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur");
                return;
            }
        }
        if (ConnexionUtilisateur::estAdministrateur() && $utilisateur === null) {
            MessageFlash::ajouter("warning", "Login inconnu");
            self::redirectionVersUrl("controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur");
            return;
        }

        if (!isset($_REQUEST["nom"], $_REQUEST["prenom"], $_REQUEST["mdp"], $_REQUEST["mdp2"])) {
            $isAdmin = ConnexionUtilisateur::estAdministrateur();
            if ($isAdmin || !isset($_REQUEST['oldMdp'])) {
                MessageFlash::ajouter("warning", "Champs manquants");
                return;
            }
        }

        if ($_REQUEST["email"] != $utilisateur->getEmail()) {
            if (!filter_var($_REQUEST["email"], FILTER_VALIDATE_EMAIL)) {
                MessageFlash::ajouter("warning", "Email non-valide");
                return;
            }
            $utilisateur->setEmailAValider($_REQUEST["email"]);
            $utilisateur->setEmail("");
            $utilisateur->setNonce(MotDePasse::genererChaineAleatoire());
            VerificationEmail::envoiEmailValidation($utilisateur);
        }

        $utilisateur->setNom($_REQUEST['nom']);
        $utilisateur->setPrenom($_REQUEST['prenom']);
        $utilisateur->setMdpHache(MotDePasse::hacher($_REQUEST['mdp']));
        if (ConnexionUtilisateur::estAdministrateur()) {
            $utilisateur->setEstAdmin(isset($_REQUEST['estAdmin']));
        }
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        MessageFlash::ajouter("success", "Utilisateur mis à jour !");
        self::redirectionVersUrl("controleurFrontal.php?action=afficherDetail&login=" . $utilisateur->getLogin() ."&controleur=utilisateur");
    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        $mdpHache = MotDePasse::hacher($tableauDonneesFormulaire['mdp']);
        $estAdmin = isset($tableauDonneesFormulaire['estAdmin']);
        if (ConnexionUtilisateur::estConnecte() && !ConnexionUtilisateur::estAdministrateur()) {
            $estAdmin = false;
        }
        $email = "";
        $emailAValider = $tableauDonneesFormulaire['email'];
        $nonce = MotDePasse::genererChaineAleatoire();
        $utilisateur = new Utilisateur($login, $nom, $prenom, $mdpHache, $estAdmin, $email, $emailAValider, $nonce);
        return $utilisateur;
    }

    /*
    public static function deposerCookie()
    {
        Cookie::enregistrer("TestCookie", 4, 3600);
    }

    public static function lireCookie()
    {
        echo Cookie::lire("TestCookie");
    }
    */

    public static function demarrerSession(): void
    {
        $session = Session::getInstance();
        $session->enregistrer("utilisateur", "Ratio uwu");
    }

    public static function lireSession(): void
    {
        $session = Session::getInstance();
        echo $session->lire("utilisateur");
    }

    public static function supprimerSession(): void
    {
        $session = Session::getInstance();
        $session->supprimer("utilisateur");
    }

    public static function supprimerTouteSession(): void
    {
        $session = Session::getInstance();
        $session->detruire();
    }

    public static function afficherFormulaireConnexion(): void
    {
        self::afficherVue("vueGenerale.php", ["titre" => "Connexion", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter(): void
    {
        if (!isset($_REQUEST['login']) || !isset($_REQUEST['mdp'])) {
            self::afficherErreur("Login et/ou mot de passe manquant(s)");
            return;
        }

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
        if ($utilisateur === null || !MotDePasse::verifier($_REQUEST['mdp'], $utilisateur->getMdpHache())) {
            self::afficherErreur("Login et/ou mot de passe incorrect(s)");
            return;
        }

        if (!VerificationEmail::aValideEmail($utilisateur)) {
            self::afficherErreur("Email non-verifié");
        }

        ConnexionUtilisateur::connecter($utilisateur->getLogin());
        MessageFlash::ajouter("success", "Utilisateur connecté !");
        self::redirectionVersUrl("controleurFrontal.php?action=afficherDetail&login=" . $utilisateur->getLogin() ."&controleur=utilisateur");
    }

    public static function deconnecter(): void
    {
        ConnexionUtilisateur::deconnecter();
        MessageFlash::ajouter("success", "Utilisateur déconnecté !");
        self::redirectionVersUrl("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
    }

    public static function validerEmail(): void {
        if (!isset($_REQUEST['login'], $_REQUEST['nonce'])) {
            self::afficherErreur("Champs manquants");
            return;
        }
        $login = $_REQUEST['login'];
        $nonce = $_REQUEST['nonce'];
        if (VerificationEmail::traiterEmailValidation($login, $nonce)) {
            self::afficherDetail();
            return;
        }
        self::afficherErreur("Problème avec la vérification");
    }
}