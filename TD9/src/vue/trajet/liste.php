<?php
/** @var Trajet[] $trajets */

use App\Covoiturage\Modele\DataObject\Trajet;

foreach ($trajets as $trajet) {
    $trajetIdHTML = htmlspecialchars($trajet->getId());
    $trajetIdURL = rawurlencode($trajet->getId());
    echo "<p> Trajet de login <a href='controleurFrontal.php?controleur=trajet&action=afficherDetail&trajetId=$trajetIdURL'>$trajetIdHTML</a>. 
    (<a href='controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&trajetId=$trajetIdURL'>Modifier</a>)
    (<a href='controleurFrontal.php?controleur=trajet&action=supprimer&trajetId=$trajetIdURL'>Supprimer</a>)</p>";
}

echo "<p> <a href='controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation'>Créer un trajet</a></p>";

?>
