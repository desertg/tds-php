<?php
/** @var Utilisateur[] $utilisateurs */

use App\Covoiturage\Modele\DataObject\Utilisateur;

foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo "<p> Utilisateur de login <a href='controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=$loginURL'>$loginHTML</a>.";
    echo "</p>";
}
?>
