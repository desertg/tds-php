<?php
/** @var \App\Covoiturage\Modele\DataObject\Utilisateur $utilisateur */

$loginURL = rawurlencode($utilisateur->getLogin());

echo '<p> Utilisateur : ' . htmlspecialchars($utilisateur) . '</p>';
echo  "(<a href='controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=$loginURL'>Modifier</a>)
    (<a href='controleurFrontal.php?controleur=utilisateur&action=supprimer&login=$loginURL'>Supprimer</a>)";
