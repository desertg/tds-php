<form method=<?php if (\App\Covoiturage\Configuration\ConfigurationSite::getDebug()) echo "get"; else echo "post" ?> action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type='hidden' name='action' value='mettreAJour'>
        <input type="hidden" name="controleur" value="utilisateur">
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text"
                   value=<?= /** @var \App\Covoiturage\Modele\DataObject\Utilisateur $utilisateur */
                   htmlspecialchars($utilisateur->getLogin()) ?> name="login" id="login_id" readonly>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text" value=<?= htmlspecialchars($utilisateur->getNom()) ?> name="nom"
                   id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom&#42;</label>
            <input class="InputAddOn-field" type="text"
                   value=<?= htmlspecialchars($utilisateur->getPrenom()) ?> name="prenom" id="prenom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="email_id">Email&#42;</label>
            <input class="InputAddOn-field" type="text"
                   value=<?= htmlspecialchars($utilisateur->getEmail()) ?> name="email" id="email_id" required/>
        </p>
        <?php
        if (!(\App\Covoiturage\Lib\ConnexionUtilisateur::estConnecte() && \App\Covoiturage\Lib\ConnexionUtilisateur::estAdministrateur())) {
            echo "
            <p class='InputAddOn'>
            <label class='InputAddOn-item' for='mdp_id' > Ancien mot de passe &#42;</label>
            <input class='InputAddOn-field' type = 'password' name = 'oldMdp' id = 'mdp_id' required >
        </p >
        ";
        }
        ?>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>
        <?php
        if ($utilisateur->isEstAdmin()) $isChecked = "checked"; else $isChecked = "";
        if (\App\Covoiturage\Lib\ConnexionUtilisateur::estConnecte() && \App\Covoiturage\Lib\ConnexionUtilisateur::estAdministrateur()) {
            echo "<p class='InputAddOn'>
            <label class='InputAddOn-item' for='estAdmin_id'>Administrateur</label>
            <input class='InputAddOn-field' type='checkbox' name='estAdmin' id='estAdmin_id' $isChecked>
        </p>";
        }
        ?>
        <p>
            <input type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>
