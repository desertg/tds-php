<form method=<?php if (\App\Covoiturage\Configuration\ConfigurationSite::getDebug()) echo "get"; else echo "post" ?> action="controleurFrontal.php">
    <input type="hidden" name="action" value="enregistrerPreference">
    <p class="InputAddOn">
    <input class="InputAddOn-field" type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
        <?php if (\App\Covoiturage\Lib\PreferenceControleur::existe() && App\Covoiturage\Lib\PreferenceControleur::lire() === "utilisateur") echo "checked"?>>
    <label class="InputAddOn-item" for="utilisateurId">Utilisateur</label>
    </p>
    <p class="InputAddOn">
    <input class="InputAddOn-field" type="radio" id="trajetId" name="controleur_defaut" value="trajet"
        <?php if (\App\Covoiturage\Lib\PreferenceControleur::existe() && \App\Covoiturage\Lib\PreferenceControleur::lire() === "trajet") echo "checked"?>>
    <label class="InputAddOn-item" for="trajetId">Trajet</label>
    </p>
    <input type="submit" value="Valider">
</form>
