<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, 'titre' => 'Liste des utilisateurs', 'cheminCorpsVue' => 'utilisateur/liste.php']); //appel à la vue
    }

    public static function afficherDetail(): void
    {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur == null) {
            self::afficherErreur("Utilisateur inconnu");
        } else {
            self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, 'titre' => 'Détails utilisateur', 'cheminCorpsVue' => 'utilisateur/detail.php']);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ['titre' => 'Créer utilisateur', 'cheminCorpsVue' => 'utilisateur/formulaireCreation.php']);
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['titre' => 'Utilisateur créé', 'cheminCorpsVue' => 'utilisateur/utilisateurCree.php', 'utilisateurs' => $utilisateurs]);
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue('vueGenerale.php', ['messageErreur' => $messageErreur, 'titre' => 'Problème utilisateur', 'cheminCorpsVue' => 'utilisateur/erreur.php']);
    }

    public static function supprimer(): void
    {
        $login = $_GET['login'];
        (new UtilisateurRepository())->supprimerParClePrimaire($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ['titre' => 'Utilisateur supprimé', 'cheminCorpsVue' => 'utilisateur/utilisateurSupprime.php', 'login' => $login, "utilisateurs" => $utilisateurs]);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        self::afficherVue("vueGenerale.php", ['titre' => 'Modifier utilisateur', 'cheminCorpsVue' => 'utilisateur/formulaireMiseAJour.php', 'utilisateur' => $utilisateur]);
    }

    public static function mettreAJour(): void
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ['titre' => 'Utilisateur mis à jour', 'cheminCorpsVue' => 'utilisateur/utilisateurMisAJour.php', 'utilisateurs' => $utilisateurs]);
    }

    /*
    public static function deposerCookie()
    {
        Cookie::enregistrer("TestCookie", 4, 3600);
    }

    public static function lireCookie()
    {
        echo Cookie::lire("TestCookie");
    }
    */
    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        $utilisateur = new Utilisateur($login, $nom, $prenom);
        return $utilisateur;
    }

    public static function demarrerSession(): void {
        $session = Session::getInstance();
        $session->enregistrer("utilisateur", "Ratio uwu");
    }

    public static function lireSession(): void {
        $session = Session::getInstance();
        echo $session->lire("utilisateur");
    }

    public static function supprimerSession(): void {
        $session = Session::getInstance();
        $session->supprimer("utilisateur");
    }

    public static function supprimerTouteSession(): void {
        $session = Session::getInstance();
        $session->detruire();
    }
}