<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="enregistrerPreference">
    <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
        <?php if (\App\Covoiturage\Lib\PreferenceControleur::lire() === "utilisateur") echo "checked"?>>
    <label for="utilisateurId">Utilisateur</label>
    <input type="radio" id="trajetId" name="controleur_defaut" value="trajet"
        <?php if (\App\Covoiturage\Lib\PreferenceControleur::lire() === "trajet") echo "checked"?>>
    <label for="trajetId">Trajet</label>
    <input type="submit" value="Valider">
</form>
