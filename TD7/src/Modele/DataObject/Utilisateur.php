<?php

namespace App\Covoiturage\Modele\DataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Trajet;

//require_once 'Trajet.php';

class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;

    private ?array $trajetsCommePassager;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom,
    )
    {
        $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString(): string
    {
        // À compléter dans le prochain exercice
        return $this->prenom . " " . $this->nom . " (" . $this->login . ")";
    }

    /**
     * @return mixed
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login)
    {
        $this->login = substr($login, 0, 64);
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    public function getTrajetsCommePassager(): ?array
    {
        return $this->trajetsCommePassager ?? UtilisateurRepository::recupererTrajetsCommePassager($this);
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

}

?>