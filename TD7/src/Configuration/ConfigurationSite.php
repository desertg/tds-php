<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    private static $tempsExpiration = 3600;

    public static function getTempsExpiration(): int
    {
        return self::$tempsExpiration;
    }


}