<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title> Mon premier php </title>
</head>

<body>
    Voici le résultat du script PHP :
    <?php

        // Ceci est un commentaire PHP sur une ligne
        /* Ceci est le 2ème type de commentaire PHP
        sur plusieurs lignes */

        // On met la chaine de caractères "hello" dans la variable 'texte'
        // Les noms de variable commencent par $ en PHP
        $texte = "hello world !";

        // On écrit le contenu de la variable 'texte' dans la page Web
        echo $texte . "\n";
        /*
        $prenom = "Marc";


        echo "Bonjour\n " . $prenom;
        echo "Bonjour\n $prenom";
        echo 'Bonjour\n $prenom';

        echo $prenom;
        echo "$prenom";
        */


        $nom = "Dupont";
        $prenom = "Marc";
        $login = "marcdupont";

        echo "<p> Utilisateur $prenom $nom de login $login </p>";

        $utilisateur = [
                "nom" => "Dupont",
                "prenom" => "Marc",
                "login" => "marcdupont"
        ];

        var_dump($utilisateur);

        echo "<p> Utilisateur " . $utilisateur["prenom"] . " " . $utilisateur["nom"] . " de login " . $utilisateur["login"] . "</p>";

        $utilisateurs = [
                "0" => [
                        "nom" => "Dupont",
                        "prenom" => "Marc",
                        "login" => "marcdupont"
                ],
                "1" => [
                        "nom" => "Durand",
                        "prenom" => "Jean",
                        "login" => "jeandurand"
                ],
                "2" => [
                        "nom" => "Dujardin",
                        "prenom" => "Marcel",
                        "login" => "marceldujardin"
                ],
        ];

        var_dump($utilisateurs);

        if (empty($utilisateurs)) {
            echo "<p> Il n'y a pas d'utilisateurs </p>";
        } else {
            echo "<h1> Liste des utilisateurs </h1>";
            echo "<ul>";
            foreach ($utilisateurs as $utilisateur) {
                echo "<li> Utilisateur " . $utilisateur["prenom"] . " " . $utilisateur["nom"] . " de login " . $utilisateur["login"] . "</li>";
            }
            echo "</ul>";
        }


    ?>
</body>
</html> 