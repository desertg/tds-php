<html>
<head>
    <title>Modifier un trajet</title>
</head>
<body>
<?php
require_once 'Trajet.php';

$trajet = Trajet::recupererTrajetParId($_GET['trajetId']);

?>

<form method="post" action="mettreAJour.php">
    <!-- Remplacer method="get" par method="post" pour changer le format d'envoi des données -->
    <fieldset>
        <legend>Modification du trajet</legend>
        <input type="hidden" value="<?php echo $trajet->getId()?>" name="trajetId">
        <p>
            <label for="depart_id">Depart</label> :
            <input type="text" value="<?php echo $trajet->getDepart() ?>" name="depart" id="depart_id" required/>
        </p>
        <p>
            <label for="arrivee_id">Arrivée</label> :
            <input type="text" value="<?php echo $trajet->getArrivee() ?>" name="arrivee" id="arrivee_id" required/>
        </p>
        <p>
            <label for="date_id">Date</label> :
            <input type="date" value="<?php echo $trajet->getDate()->format("Y-m-d") ?>" name="date" id="date_id" required/>
        </p>
        <p>
            <label for="prix_id">Prix</label> :
            <input type="int" value="<?php echo $trajet->getPrix() ?>" name="prix" id="prix_id" required/>
        </p>
        <p>
            <label for="conducteurLogin_id">Login du conducteur</label> :
            <input type="text" value="<?php echo $trajet->getConducteur()->getLogin() ?>" name="conducteurLogin" id="conducteurLogin_id"
                   required/>
        </p>
        <p>
            <label for="nonFumeur_id">Non Fumeur ?</label> :
            <input type="checkbox" checked="<?php echo $trajet->isNonFumeur() ? 1 : 0 ?>" name="nonFumeur"
                   id="nonFumeur_id"/>
        </p>
        <p>
            <input type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>
</body>
</html>



