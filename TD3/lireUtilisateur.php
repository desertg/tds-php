<?php

require_once "Utilisateur.php";

$pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
//$utilisateurFormatTableau = $pdoStatement->fetch();
//echo var_dump($utilisateurFormatTableau);

//$utilisateur = new Utilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
//echo $utilisateur;

foreach (Utilisateur::recupererUtilisateurs() as $utilisateur) {
    echo "<p> $utilisateur </p>";
    foreach ($utilisateur->getTrajetsCommePassager() as $trajet) {
        echo "<p> Passager du trajet : $trajet </p>";
    }
    foreach ($utilisateur->getTrajetsCommeConducteur() as $trajet) {
        echo "<p> Conducteur du trajet : $trajet </p>";
    }
    echo "<p> ---------------------------------- </p>";
}