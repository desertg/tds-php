<?php

require_once 'Trajet.php';

$depart = $_POST['depart'];
$arrivee = $_POST['arrivee'];
$date = $_POST['date'];
$prix = $_POST['prix'];
$conducteurLogin = $_POST['conducteurLogin'];
$nonFumeur = isset($_POST['nonFumeur']) ? 1 : 0;
$id = $_POST['trajetId'];

$sql = "UPDATE trajet 
        SET depart = :departTag, 
            arrivee = :arriveeTag, 
            date = :dateTag, 
            prix = :prixTag, 
            conducteur = :conducteurTag, 
            nonFumeur = :nonFumeurTag 
        WHERE id = :idTag";
$pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);

$values = array(
    "departTag" => $depart,
    "arriveeTag" => $arrivee,
    "dateTag" => $date,
    "prixTag" => $prix,
    "conducteurTag" => $conducteurLogin,
    "nonFumeurTag" => $nonFumeur,
    "idTag" => $id
);
$pdoStatement->execute($values);