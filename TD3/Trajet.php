<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

class Trajet
{

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;
    private array $passagers;

    public function __construct(
        ?int        $id,
        string      $depart,
        string      $arrivee,
        DateTime    $date,
        int         $prix,
        Utilisateur $conducteur,
        bool        $nonFumeur,
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = [];
    }

    public static function construireDepuisTableauSQL(array $trajetTableau): Trajet
    {
        return new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À changer
            $trajetTableau["prix"],
            Utilisateur::recupererUtilisateurParLogin($trajetTableau["conducteur"]), // À changer
            $trajetTableau["nonFumeur"], // À changer ?
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function getPassagers(): array
    {
        return $this->recupererPassagers();
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach ($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function ajouter(): void
    {
        $sql = "INSERT INTO trajet (depart, arrivee, date, prix, conducteur, nonFumeur) VALUES (:departTag, :arriveeTag, :dateTag, :prixTag, :conducteurTag, :nonFumeurTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);

        $values = array(
            "departTag" => $this->depart,
            "arriveeTag" => $this->arrivee,
            "dateTag" => $this->date->format("Y-m-d"),
            "prixTag" => $this->prix,
            "conducteurTag" => $this->conducteur->getLogin(),
            "nonFumeurTag" => $this->nonFumeur ? 1 : 0
        );
        $pdoStatement->execute($values);
        ConnexionBaseDeDonnees::getPdo()->lastInsertId();
    }

    /**
     * @return Utilisateur[]
     */
    private function recupererPassagers(): array
    {
        $sql = "SELECT * FROM utilisateur WHERE login IN (SELECT passagerLogin FROM passager WHERE trajetId = :trajetTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);

        $values = array(
            "trajetTag" => $this->id
        );
        $pdoStatement->execute($values);

        foreach ($pdoStatement as $passagerFormatTableau) {
            $this->passagers[] = Utilisateur::construireDepuisTableauSQL($passagerFormatTableau);
        }

        return $this->passagers;
    }

    public static function recupererTrajetParId(int $id): ?Trajet
    {
        $sql = "SELECT * FROM trajet WHERE id = :idTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);

        $values = array(
            "idTag" => $id
        );
        $pdoStatement->execute($values);

        $trajetFormatTableau = $pdoStatement->fetch();

        if (empty($trajetFormatTableau)) {
            return null;
        }

        return Trajet::construireDepuisTableauSQL($trajetFormatTableau);
    }

    public function supprimerPassager(string $passagerLogin): bool
    {
        $nbLignes = (new PDOStatement)->rowCount();
        $sql = "DELETE FROM passager WHERE passagerLogin = :passagerLoginTag AND trajetId = :trajetIdTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            'passagerLoginTag' => $passagerLogin,
            'trajetIdTag' => $this->id
        );
        $pdoStatement->execute($values);
        if ($pdoStatement->rowCount() == 0) {
            return false;
        }
        return true;

    }

}
