<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Trajet.php';

class Utilisateur
{

    private string $login;
    private string $nom;
    private string $prenom;

    private ?array $trajetsCommePassager;
    private ?array $trajetsCommeConducteur;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom,
    )
    {
        $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString(): string
    {
        // À compléter dans le prochain exercice
        return $this->prenom . " " . $this->nom . " (" . $this->login . ")";
    }

    /**
     * @return mixed
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login)
    {
        $this->login = substr($login, 0, 64);
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    public function getTrajetsCommePassager(): ?array
    {
        return $this->trajetsCommePassager ?? $this->recupererTrajetsCommePassager();
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    public function getTrajetsCommeConducteur(): ?array
    {
        return $this->trajetsCommeConducteur ?? $this->recupererTrajetsCommeConducteur();
    }

    public function setTrajetsCommeConducteur(?array $trajetsCommeConducteur): void
    {
        $this->trajetsCommeConducteur = $trajetsCommeConducteur;
    }



    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    public static function recupererUtilisateurs(): array
    {
        require_once "ConnexionBaseDeDonnees.php";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        $utilisateurs = array();
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login): ?Utilisateur
    {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if (empty($utilisateurFormatTableau)) {
            return null;
        }

        return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter(): bool
    {
        try {
            $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:loginTag, :nomTag, :prenomTag)";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

            $values = array(
                "loginTag" => $this->login,
                "nomTag" => $this->nom,
                "prenomTag" => $this->prenom
            );

            $pdoStatement->execute($values);
            return true;
        } catch (PDOException $e) {
            return false;
        }

    }

    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager(): array
    {
        $sql = "SELECT * FROM trajet WHERE id IN (SELECT trajetId FROM passager WHERE passagerLogin = :loginTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $this->login
        );
        $pdoStatement->execute($values);

        $trajets = [];
        foreach ($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }
        return $trajets;
    }

    private function recupererTrajetsCommeConducteur(): array
    {
        $sql = 'SELECT * FROM trajet WHERE conducteur = :conducteurTag';
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "conducteurTag" => $this->login
        );
        $pdoStatement->execute($values);

        $trajets = [];
        foreach ($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }
        return $trajets;
    }

}

?>