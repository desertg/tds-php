<?php

require_once "Trajet.php";

$pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM trajet");

foreach (Trajet::recupererTrajets() as $trajet) {
    $trajetId = $trajet->getId();

    echo "<p> $trajet <a href='mettreAJourTrajet.php?trajetId=$trajetId'>Mettre à jour ?</a></p>";
    foreach ($trajet->getPassagers() as $passager) {
        $login = $passager->getLogin();
        $trajet = $trajet->getId();
        echo "<p> - $passager <a href='supprimerPassager.php?passagerLogin=$login&trajetId=$trajet'>Désinscrire ?</a></p>";
    }
    echo "<p> ---------------------------------- </p>";
}
