<?php

require_once 'Trajet.php';
$depart = $_POST["depart"];
$arrivee = $_POST["arrivee"];
$date = $_POST["date"];
$prix = $_POST["prix"];
$conducteur = $_POST["conducteurLogin"];
$nonFumeur = isset($_POST["nonFumeur"]);

if (!empty($_POST)) {
    $trajet = new Trajet(null, $depart, $arrivee, new DateTime($date), $prix, Utilisateur::recupererUtilisateurParLogin($conducteur), $nonFumeur);
    $trajet->ajouter();
    echo "<p> $trajet </p>";
} else {
    echo "<p> Il n'y a pas d'utilisateurs </p>";
}
