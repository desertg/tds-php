<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDOException;

class UtilisateurRepository extends AbstractRepository
{
    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        /** @var Utilisateur $objet */
        return array(
            "loginTag" => $objet->getLogin(),
            "nomTag" => $objet->getNom(),
            "prenomTag" => $objet->getPrenom(),
            "mdpHacheTag" => $objet->getMdpHache(),
            "estAdminTag" => $objet->isEstAdmin() ? 1 : 0,
            "emailTag" => $objet->getEmail(),
            "emailAValiderTag" => $objet->getEmailAValider(),
            "nonceTag" => $objet->getNonce()
        );
    }
    protected function getNomColonnes(): array
    {
        return ["login", "nom", "prenom", "mdpHache", "estAdmin", "email", "emailAValider", "nonce"];
    }

    protected function getNomClePrimaire() : string {
        return 'login';
    }
    protected function getNomTable(): string
    {
        return 'utilisateur';
    }

    public function construireDepuisTableauSQL(array $objetFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $objetFormatTableau['login'],
            $objetFormatTableau['nom'],
            $objetFormatTableau['prenom'],
            $objetFormatTableau['mdpHache'],
            $objetFormatTableau['estAdmin'],
            $objetFormatTableau['email'],
            $objetFormatTableau['emailAValider'],
            $objetFormatTableau['nonce']
        );
    }

    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur): array
    {
        $sql = "SELECT * FROM trajet WHERE id IN (SELECT trajetId FROM passager WHERE passagerLogin = :loginTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $utilisateur->getLogin()
        );
        $pdoStatement->execute($values);

        $trajets = [];
        foreach ($pdoStatement as $trajetFormatTableau) {
            $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajetFormatTableau);
        }
        return $trajets;
    }

}