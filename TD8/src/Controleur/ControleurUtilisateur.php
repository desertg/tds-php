<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, 'titre' => 'Liste des utilisateurs', 'cheminCorpsVue' => 'utilisateur/liste.php']); //appel à la vue
    }

    public static function afficherDetail(): void
    {
        $login = $_REQUEST['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur == null) {
            self::afficherErreur("Utilisateur inconnu");
        } else {
            self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, 'titre' => 'Détails utilisateur', 'cheminCorpsVue' => 'utilisateur/detail.php']);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ['titre' => 'Créer utilisateur', 'cheminCorpsVue' => 'utilisateur/formulaireCreation.php']);
    }

    public static function creerDepuisFormulaire(): void
    {
        if ($_REQUEST['mdp2'] !== $_REQUEST['mdp']) {
            self::afficherErreur("Mots de passe distincts");
            return;
        }

        if (isset($_REQUEST["nom"], $_REQUEST["prenom"], $_REQUEST["mdp"], $_REQUEST["mdp2"], $_REQUEST["email"]) && filter_var($_REQUEST["email"], FILTER_VALIDATE_EMAIL)) {

            $utilisateur = self::construireDepuisFormulaire($_REQUEST);
            (new UtilisateurRepository())->ajouter($utilisateur);
            VerificationEmail::envoiEmailValidation($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ['titre' => 'Utilisateur créé', 'cheminCorpsVue' => 'utilisateur/utilisateurCree.php', 'utilisateurs' => $utilisateurs]);
            return;

        }
        self::afficherErreur("Champs manquant");
    }


    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue('vueGenerale.php', ['messageErreur' => $messageErreur, 'titre' => 'Problème utilisateur', 'cheminCorpsVue' => 'utilisateur/erreur.php']);
    }

    public static function supprimer(): void
    {
        $login = $_REQUEST['login'];

        if (!ConnexionUtilisateur::estUtilisateur($login)) {
            self::afficherErreur("La suppression n'est possible que pour l'utilisateur connecté");
            return;
        }
        ConnexionUtilisateur::deconnecter();
        (new UtilisateurRepository())->supprimerParClePrimaire($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ['titre' => 'Utilisateur supprimé', 'cheminCorpsVue' => 'utilisateur/utilisateurSupprime.php', 'login' => $login, "utilisateurs" => $utilisateurs]);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
        if ($utilisateur === null && ConnexionUtilisateur::estAdministrateur()) {
            self::afficherErreur("Login inconnu");
        }
        if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin()) || ConnexionUtilisateur::estAdministrateur()) {
            self::afficherVue("vueGenerale.php", ['titre' => 'Modifier utilisateur', 'cheminCorpsVue' => 'utilisateur/formulaireMiseAJour.php', 'utilisateur' => $utilisateur]);
            return;
        }
        self::afficherErreur("La mise à jour n'est possible que pour l'utilisateur connecté");
    }

    public static function mettreAJour(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);

        if (!ConnexionUtilisateur::estConnecte() ||
            (!ConnexionUtilisateur::estAdministrateur() && !ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin()))) {
            self::afficherErreur("La mise à jour n'est possible que pour l'utilisateur connecté");
            return;
        }


        if ($_REQUEST['mdp'] !== $_REQUEST['mdp2']) {
            self::afficherErreur("Mots de passe distincts");
            return;
        }

        if (ConnexionUtilisateur::estConnecte() && !ConnexionUtilisateur::estAdministrateur()) {
            if (!MotDePasse::verifier($_REQUEST['oldMdp'], $utilisateur->getMdpHache())) {
                self::afficherErreur("Ancien mot de passe incorrect");
                return;
            }
        }
        if (ConnexionUtilisateur::estAdministrateur() && $utilisateur === null) {
            self::afficherErreur("Login inconnu");
            return;
        }

        if (!isset($_REQUEST["nom"], $_REQUEST["prenom"], $_REQUEST["mdp"], $_REQUEST["mdp2"])) {
            $isAdmin = ConnexionUtilisateur::estAdministrateur();
            if ($isAdmin || !isset($_REQUEST['oldMdp'])) {
                self::afficherErreur("Champs manquant");
                return;
            }
        }

        if ($_REQUEST["email"] != $utilisateur->getEmail()) {
            if (!filter_var($_REQUEST["email"], FILTER_VALIDATE_EMAIL)) {
                self::afficherErreur("Email non-valide");
                return;
            }
            $utilisateur->setEmailAValider($_REQUEST["email"]);
            $utilisateur->setEmail("");
            $utilisateur->setNonce(MotDePasse::genererChaineAleatoire());
            VerificationEmail::envoiEmailValidation($utilisateur);
        }

        $utilisateur->setNom($_REQUEST['nom']);
        $utilisateur->setPrenom($_REQUEST['prenom']);
        $utilisateur->setMdpHache(MotDePasse::hacher($_REQUEST['mdp']));
        if (ConnexionUtilisateur::estAdministrateur()) {
            $utilisateur->setEstAdmin(isset($_REQUEST['estAdmin']));
        }
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ['titre' => 'Utilisateur mis à jour', 'cheminCorpsVue' => 'utilisateur/utilisateurMisAJour.php', 'utilisateurs' => $utilisateurs]);
    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        $mdpHache = MotDePasse::hacher($tableauDonneesFormulaire['mdp']);
        $estAdmin = isset($tableauDonneesFormulaire['estAdmin']);
        if (ConnexionUtilisateur::estConnecte() && !ConnexionUtilisateur::estAdministrateur()) {
            $estAdmin = false;
        }
        $email = "";
        $emailAValider = $tableauDonneesFormulaire['email'];
        $nonce = MotDePasse::genererChaineAleatoire();
        $utilisateur = new Utilisateur($login, $nom, $prenom, $mdpHache, $estAdmin, $email, $emailAValider, $nonce);
        return $utilisateur;
    }

    /*
    public static function deposerCookie()
    {
        Cookie::enregistrer("TestCookie", 4, 3600);
    }

    public static function lireCookie()
    {
        echo Cookie::lire("TestCookie");
    }
    */

    public static function demarrerSession(): void
    {
        $session = Session::getInstance();
        $session->enregistrer("utilisateur", "Ratio uwu");
    }

    public static function lireSession(): void
    {
        $session = Session::getInstance();
        echo $session->lire("utilisateur");
    }

    public static function supprimerSession(): void
    {
        $session = Session::getInstance();
        $session->supprimer("utilisateur");
    }

    public static function supprimerTouteSession(): void
    {
        $session = Session::getInstance();
        $session->detruire();
    }

    public static function afficherFormulaireConnexion(): void
    {
        self::afficherVue("vueGenerale.php", ["titre" => "Connexion", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter(): void
    {
        if (!isset($_REQUEST['login']) || !isset($_REQUEST['mdp'])) {
            self::afficherErreur("Login et/ou mot de passe manquant(s)");
            return;
        }

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
        if ($utilisateur === null || !MotDePasse::verifier($_REQUEST['mdp'], $utilisateur->getMdpHache())) {
            self::afficherErreur("Login et/ou mot de passe incorrect(s)");
            return;
        }

        if (!VerificationEmail::aValideEmail($utilisateur)) {
            self::afficherErreur("Email non-verifié");
        }

        ConnexionUtilisateur::connecter($utilisateur->getLogin());
        self::afficherVue("vueGenerale.php", ["titre" => "Connexion réussie !", "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php", "utilisateur" => $utilisateur]);
    }

    public static function deconnecter(): void
    {
        ConnexionUtilisateur::deconnecter();
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["titre" => "Déconnexion réussie !", "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php", "utilisateurs" => $utilisateurs]);
    }

    public static function validerEmail(): void {
        if (!isset($_REQUEST['login'], $_REQUEST['nonce'])) {
            self::afficherErreur("Champs manquants");
            return;
        }
        $login = $_REQUEST['login'];
        $nonce = $_REQUEST['nonce'];
        if (VerificationEmail::traiterEmailValidation($login, $nonce)) {
            self::afficherDetail();
            return;
        }
        self::afficherErreur("Problème avec la vérification");
    }
}