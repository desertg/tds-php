<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Configuration\ConfigurationSite;
use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class VerificationEmail
{
    public static function envoiEmailValidation(Utilisateur $utilisateur): void
    {
        $destinataire = $utilisateur->getEmailAValider();
        $sujet = "Validation de l'adresse email";
        // Pour envoyer un email contenant du HTML
        $enTete = "MIME-Version: 1.0\r\n";
        $enTete .= "Content-type:text/html;charset=UTF-8\r\n";

        // Corps de l'email
        $loginURL = rawurlencode($utilisateur->getLogin());
        $nonceURL = rawurlencode($utilisateur->getNonce());
        $URLAbsolue = ConfigurationSite::getURLAbsolue();
        $lienValidationEmail = "$URLAbsolue?action=validerEmail&controleur=utilisateur&login=$loginURL&nonce=$nonceURL";
        $corpsEmailHTML = "<a href=\"$lienValidationEmail\">Validation</a>";

        // Temporairement avant d'envoyer un vrai mail
        echo "Simulation d'envoi d'un mail<br> Destinataire : $destinataire<br> Sujet : $sujet<br> Corps : <br>$corpsEmailHTML";

        // Quand vous aurez configué l'envoi de mail via PHP
        // mail($destinataire, $sujet, $corpsEmailHTML, $enTete);
    }

    public static function traiterEmailValidation($login, $nonce): bool
    {
        // À compléter
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur->getNonce() == $nonce) {
            $utilisateur->setNonce("");
            $utilisateur->setEmail($utilisateur->getEmailAValider());
            $utilisateur->setEmailAValider("");
            (new UtilisateurRepository())->mettreAJour($utilisateur);
            return true;
        }
        return false;
    }

    public static function aValideEmail(Utilisateur $utilisateur) : bool
    {
        if ($utilisateur->getNonce() == "" && $utilisateur->getEmailAValider() == "" && !($utilisateur->getEmail() == "")) {
            return true;
        }
        return false;
    }
}