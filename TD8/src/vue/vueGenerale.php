<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../ressources/css/styles.css">
    <title><?php /** @var string $titre */
        echo $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href='controleurFrontal.php?action=afficherFormulairePreference'><img
                            src='../ressources/img/heart.png' alt='Préférences'></a>
            </li>
            <?php
            if (!\App\Covoiturage\Lib\ConnexionUtilisateur::estConnecte()) {
                echo "<li>
                <a href='controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur'><img src='../ressources/img/add-user.png' alt='Add user'></a>
            </li><li>
                <a href='controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur'><img src='../ressources/img/enter.png' alt='Se connecter'></a>
            </li>";
            } else {
                $login = \App\Covoiturage\Lib\ConnexionUtilisateur::getLoginUtilisateurConnecte();
                echo "<li>
                <a href='controleurFrontal.php?action=afficherDetail&login=" . rawurlencode($login) . "&controleur=utilisateur'><img src='../ressources/img/user.png' alt='Add user'></a>
            </li><li>
                <a href='controleurFrontal.php?action=deconnecter&controleur=utilisateur'><img src='../ressources/img/logout.png' alt='Se connecter'></a>
            </li>";
                if (\App\Covoiturage\Lib\ConnexionUtilisateur::estAdministrateur()) {
                    echo "<li>
                <a href='controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur'><img src='../ressources/img/add-user.png' alt='Add user'></a>
            </li>";
                }
            }
            ?>
        </ul>
    </nav>
</header>
<main>
    <?php
    /** @var string $cheminCorpsVue */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        © 2024 - Site de covoiturage de Galdric
    </p>
</footer>
</body>
</html>