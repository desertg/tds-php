<form method=<?php if (\App\Covoiturage\Configuration\ConfigurationSite::getDebug()) echo "get"; else echo "post" ?> action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type='hidden' name='action' value='mettreAJour'>
        <input type="hidden" name='controleur' value='trajet'>
        <input type="hidden" name="trajetId" value="<?= /** @var \App\Covoiturage\Modele\DataObject\Trajet $trajet */ htmlspecialchars($trajet->getId()) ?>">
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Depart</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($trajet->getDepart())?>" name="depart" id="depart_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivée</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($trajet->getArrivee()) ?>" name="arrivee" id="arrivee_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date</label>
            <input class="InputAddOn-field" type="date" value="<?= htmlspecialchars($trajet->getDate()->format("Y-m-d")) ?>" name="date" id="date_id"  required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix</label>
            <input class="InputAddOn-field" type="int" value="<?= htmlspecialchars($trajet->getPrix()) ?>" name="prix" id="prix_id"  required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteurLogin_id">Login du conducteur</label> :
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($trajet->getConducteur()->getLogin()) ?>" name="conducteurLogin" id="conducteurLogin_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non Fumeur ?</label>
            <input class="InputAddOn-field" type="checkbox" name="nonFumeur" id="nonFumeur_id" <?php if ($trajet->isNonFumeur()) echo "checked"?>/>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
