<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
/** @var ModeleUtilisateur[] $utilisateurs */

foreach ($utilisateurs as $utilisateur) {
    $login = $utilisateur->getLogin();
    echo "<p> Utilisateur de login <a href='../web/controleurFrontal.php?action=afficherDetail&login=$login'>$login</a>.</p>";
}

echo "<p> <a href='../web/controleurFrontal.php?action=afficherFormulaireCreation'>Créer un utilisateur</a></p>";

?>
</body>
</html>