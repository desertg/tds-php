<?php
require_once __DIR__ . '/../Modele/ModeleUtilisateur.php'; // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]); //appel à la vue
    }

    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);
        if ($utilisateur == null) {
            self::afficherVue('utilisateur/erreur.php', ['message' => "Problème avec l'utilisateur"]);
        } else {
            self::afficherVue('utilisateur/detail.php', ['utilisateur' => $utilisateur]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire() : void {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        self::afficherListe();
    }
}
?>