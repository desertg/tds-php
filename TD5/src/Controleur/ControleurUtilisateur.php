<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\ModeleUtilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, 'titre' => 'Liste des utilisateurs', 'cheminCorpsVue' => 'utilisateur/liste.php']); //appel à la vue
    }

    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);
        if ($utilisateur == null) {
            self::afficherVue('vueGenerale.php', ['message' => "Problème avec l'utilisateur", 'titre' => 'Problème utilisateur', 'cheminCorpsVue' => 'utilisateur/erreur.php']);
        } else {
            self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, 'titre' => 'Détails utilisateur', 'cheminCorpsVue' => 'utilisateur/detail.php']);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php', ['titre' => 'Créer utilisateur', 'cheminCorpsVue' => 'utilisateur/formulaireCreation.php']);
    }

    public static function creerDepuisFormulaire() : void {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue('vueGenerale.php', ['titre' => 'Utilisateur créé', 'cheminCorpsVue' => 'utilisateur/utilisateurCree.php', 'utilisateurs' => $utilisateurs]);
    }
}
?>