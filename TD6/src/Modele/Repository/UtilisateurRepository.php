<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDOException;

class UtilisateurRepository extends AbstractRepository
{
    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        /** @var Utilisateur $objet */
        return array(
            "loginTag" => $objet->getLogin(),
            "nomTag" => $objet->getNom(),
            "prenomTag" => $objet->getPrenom(),
        );
    }
    protected function getNomColonnes(): array
    {
        return ["login", "nom", "prenom"];
    }

    protected function getNomClePrimaire() : string {
        return 'login';
    }
    protected function getNomTable(): string
    {
        return 'utilisateur';
    }

    public function construireDepuisTableauSQL(array $objetFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $objetFormatTableau['login'],
            $objetFormatTableau['nom'],
            $objetFormatTableau['prenom']
        );
    }

    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur): array
    {
        $sql = "SELECT * FROM trajet WHERE id IN (SELECT trajetId FROM passager WHERE passagerLogin = :loginTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $utilisateur->getLogin()
        );
        $pdoStatement->execute($values);

        $trajets = [];
        foreach ($pdoStatement as $trajetFormatTableau) {
            $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajetFormatTableau);
        }
        return $trajets;
    }

}