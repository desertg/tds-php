<?php

namespace App\Covoiturage\Controleur;


use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet
{
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['trajets' => $trajets, 'titre' => 'Liste des trajets', 'cheminCorpsVue' => 'trajet/liste.php']); //appel à la vue
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        self::afficherVue('vueGenerale.php', ['messageErreur' => $messageErreur, 'titre' => 'Problème de trajet', 'cheminCorpsVue' => 'trajet/erreur.php']);
    }

    public static function afficherDetail() : void {
        $id = $_GET['trajetId'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if ($trajet == null) {
            self::afficherErreur("Trajet inconnu");
        } else {
            self::afficherVue('vueGenerale.php', ['trajet' => $trajet, 'titre' => 'Détails trajet', 'cheminCorpsVue' => 'trajet/detail.php']);
        }
    }

    public static function supprimer() : void {
        $id = $_GET['trajetId'];
        (new TrajetRepository())->supprimerParClePrimaire($id);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ['titre' => 'Trajet supprimé', 'cheminCorpsVue' => 'trajet/trajetSupprime.php', 'id' => $id, "trajets" => $trajets]);
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php', ['titre' => 'Créer trajet', 'cheminCorpsVue' => 'trajet/formulaireCreation.php']);
    }

    public static function creerDepuisFormulaire() : void {
        $depart = $_GET["depart"];
        $arrivee = $_GET["arrivee"];
        $date = $_GET["date"];
        $prix = $_GET["prix"];
        $conducteur = $_GET["conducteurLogin"];
        $nonFumeur = isset($_GET["nonFumeur"]);
        $trajet = new Trajet(null, $depart, $arrivee, new DateTime($date), $prix, (new UtilisateurRepository())->recupererParClePrimaire($conducteur), $nonFumeur);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ['titre' => 'Trajet créé', 'cheminCorpsVue' => 'trajet/trajetCree.php', 'trajets' => $trajets]);
    }
}