<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, 'titre' => 'Liste des utilisateurs', 'cheminCorpsVue' => 'utilisateur/liste.php']); //appel à la vue
    }

    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur == null) {
            self::afficherErreur("Utilisateur inconnu");
        } else {
            self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, 'titre' => 'Détails utilisateur', 'cheminCorpsVue' => 'utilisateur/detail.php']);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php', ['titre' => 'Créer utilisateur', 'cheminCorpsVue' => 'utilisateur/formulaireCreation.php']);
    }

    public static function creerDepuisFormulaire() : void {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new Utilisateur($login, $nom, $prenom);
        (new UtilisateurRepository())->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['titre' => 'Utilisateur créé', 'cheminCorpsVue' => 'utilisateur/utilisateurCree.php', 'utilisateurs' => $utilisateurs]);
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        self::afficherVue('vueGenerale.php', ['messageErreur' => $messageErreur, 'titre' => 'Problème utilisateur', 'cheminCorpsVue' => 'utilisateur/erreur.php']);
    }

    public static function supprimer() : void {
        $login = $_GET['login'];
        (new UtilisateurRepository())->supprimerParClePrimaire($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ['titre' => 'Utilisateur supprimé', 'cheminCorpsVue' => 'utilisateur/utilisateurSupprime.php', 'login' => $login, "utilisateurs" => $utilisateurs]);
    }

    public static function afficherFormulaireMiseAJour() : void {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        self::afficherVue("vueGenerale.php", ['titre' => 'Modifier utilisateur', 'cheminCorpsVue' => 'utilisateur/formulaireMiseAJour.php', 'utilisateur' => $utilisateur]);
    }

    public static function mettreAJour() : void {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new Utilisateur($login, $nom, $prenom);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ['titre' => 'Utilisateur mis à jour', 'cheminCorpsVue' => 'utilisateur/utilisateurMisAJour.php', 'utilisateurs' => $utilisateurs]);
    }
}
?>