<?php

require __DIR__ . '/../src/Lib/psr4AutoloaderClass.php';
use App\Covoiturage\Controleur\ControleurUtilisateur;

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

// On récupère l'action passée dans l'URL
if (isset($_GET['action'])) {
    if (!in_array($_GET['action'], get_class_methods("App\Covoiturage\Controleur\ControleurUtilisateur"))) {
        $action = 'AfficherErreur';
    } else {
        $action = $_GET['action'];
    }
} else  {
    $action = 'AfficherListe';
}

if (isset($_GET['controleur'])) {
    $controleur = $_GET['controleur'];
    $nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur" . ucfirst($controleur);
    if (!class_exists($nomDeClasseControleur)) {
        App\Covoiturage\Controleur\ControleurUtilisateur::afficherErreur();
    } else {
        $nomDeClasseControleur::$action();
    }
} else {
    App\Covoiturage\Controleur\ControleurUtilisateur::$action();
}

?>